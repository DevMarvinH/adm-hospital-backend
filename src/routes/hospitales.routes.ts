/**
 * Path: /api/hospitales
 */
import { Router } from "express";
import { check } from "express-validator";
import { validarCampos } from "../middlewares/validar-campos";
import { validarJWT } from "../middlewares/validar-jwt";
import { actualizarHospital, borrarHospital, crearHospital, getHospitales } from "../controllers/hospital.controller";

const router = Router();

router.get('/', getHospitales);
//Luego de la ruta '/', declaramos los middlewares, en caso de ser uno no tendríamos que meterlo en un arreglo
//Estos middlewares, se ejecutarán antes de llegar al método del controlador
//Revisar la documentación de express-validator para las validaciones utilizadas acá
router.post('/', [
    validarJWT,
    check('nombre', 'El campo nombre es requerido').not().isEmpty(),
    validarCampos
], crearHospital);

router.put('/:id', [],
actualizarHospital);

router.delete('/:id', borrarHospital);

export default router;