/**
 * Ruta: /api/usuarios
 */
import { Router } from "express";
import { actualizarUsuario, borrarUsuario, crearUsuario, getUsuarios } from "../controllers/usuario.controller";
import { check } from "express-validator";
import { validarCampos } from "../middlewares/validar-campos";
import { validarJWT } from "../middlewares/validar-jwt";

const router = Router();

router.get('/', validarJWT, getUsuarios);
//Luego de la ruta '/', declaramos los middlewares, en caso de ser uno no tendríamos que meterlo en un arreglo
//Estos middlewares, se ejecutarán antes de llegar al método del controlador
//Revisar la documentación de express-validator para las validaciones utilizadas acá
router.post('/', [
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('password', 'La contraseña es obligatorio').not().isEmpty(),
    check('email', 'El email es obligatorio').isEmail(),
    validarCampos //Este debe de ser el último en ejecutarse, porque el recoge el resultado de las validaciones
], crearUsuario);

router.put('/:id', [
    validarJWT, //Hacemos la verificación del token antes para no ejecutar procesos demás
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('email', 'El email es obligatorio').isEmail(),
    check('role', 'El role es obligatorio').not().isEmpty(),
    validarCampos //Este debe de ser el último en ejecutarse, porque el recoge el resultado de las validaciones
],
actualizarUsuario);

router.delete('/:id', validarJWT, borrarUsuario);

export default router;