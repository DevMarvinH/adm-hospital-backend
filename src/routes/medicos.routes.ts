/**
 * Path: /api/medicos
 */
import { Router } from "express";
import { check } from "express-validator";
import { validarCampos } from "../middlewares/validar-campos";
import { validarJWT } from "../middlewares/validar-jwt";
import { actualizarMedico, borrarMedico, crearMedico, getMedicos } from "../controllers/medico.controller";

const router = Router();

router.get('/', getMedicos);
//Luego de la ruta '/', declaramos los middlewares, en caso de ser uno no tendríamos que meterlo en un arreglo
//Estos middlewares, se ejecutarán antes de llegar al método del controlador
//Revisar la documentación de express-validator para las validaciones utilizadas acá
router.post('/', [
    validarJWT,
    check('nombre', 'El campo nombre es requerido').notEmpty(),
    check('hospital', 'El campo hospital id es requerido').isMongoId(), //Validamos que el id sea válido
    validarCampos
], crearMedico);

router.put('/:id', [],
actualizarMedico);

router.delete('/:id', borrarMedico);

export default router;