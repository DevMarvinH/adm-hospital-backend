/**
 * Ruta: /api/login
 */
import { Router } from "express";
import { googleSignIn, login } from "../controllers/auth.controller";
import { check } from "express-validator";
import { validarCampos } from "../middlewares/validar-campos";

const router = Router();

router.post('/', [
    check('email', 'El campo email es requerido').isEmail(),
    check('password', 'El campo contraseña es requerida').not().isEmpty(),
    validarCampos
], login);

router.post('/google', [
    check('token', 'El token de google es obligatorio').notEmpty(),
    validarCampos
], googleSignIn);

export default router;