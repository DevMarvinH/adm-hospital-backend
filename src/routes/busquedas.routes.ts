/** 
 * Path: /api/todo/:busqueda
 */

import { Router } from "express";
import { getDocumentoColeccion, getTodo } from "../controllers/busquedas.controller";
import { validarJWT } from "../middlewares/validar-jwt";

const router = Router();

router.get('/:busqueda', [
    validarJWT
], getTodo);

router.get('/coleccion/:tabla/:busqueda', [
    validarJWT
], getDocumentoColeccion);

export default router;