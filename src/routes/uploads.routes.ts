/** 
 * Path: /api/uploads/
 */

import { Router } from "express";
import { validarJWT } from "../middlewares/validar-jwt";
import { fileUpload, obtenerImagen } from "../controllers/uploads.controller";
import * as expressFileUpload from 'express-fileupload';

const router = Router();
router.use(expressFileUpload());

router.put('/:tipo/:id', [
    validarJWT
], fileUpload);

router.get('/:tipo/:foto', [
    validarJWT
], obtenerImagen);

export default router;