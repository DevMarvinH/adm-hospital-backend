import * as express from 'express';
import * as cors from 'cors';
import * as dotenv from 'dotenv';
dotenv.config();
import { dbConnection } from './database/config';
import usuarios from './routes/usuarios.routes';
import hospitales from './routes/hospitales.routes';
import medicos from './routes/medicos.routes';
import busquedas from './routes/busquedas.routes';
import uploads from './routes/uploads.routes';
import auth from './routes/auth.routes';

// Crear el servidor de express
const app = express();

// Configurar CORS
app.use(cors());

// Carpeta publica
app.use(express.static('./src/public'));

// Lectura y parseo del body
app.use(express.json());

// Base de datos
dbConnection();

// Rutas
app.use('/api/usuarios', usuarios);
app.use('/api/hospitales', hospitales);
app.use('/api/medicos', medicos);
app.use('/api/todo', busquedas);
app.use('/api/upload', uploads);
app.use('/api/login', auth);

app.listen(process.env.PORT, () => {
    console.log('Servidor corriendo en puerto ', process.env.PORT);
});