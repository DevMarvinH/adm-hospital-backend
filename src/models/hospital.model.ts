import { Schema, model } from 'mongoose'

// En collection definimos como queremos que se cree el nombre de la tabla, ya que por el contrario Mongo solo agregaría 's'
const HospitalSchema = new Schema({
    nombre: {
        type: String,
        required: true
    },
    img: String,
    usuario: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'Usuario'
    }
}, { collection: 'hospitales' });

// Configuración visual para _id. Esto no afecta la base de datos
HospitalSchema.method('toJSON', function() {
    const { __v, ...object } = this.toObject();
    return object;
});

export const Hospital = model('Hospital', HospitalSchema);