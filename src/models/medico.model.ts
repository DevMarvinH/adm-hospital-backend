import { Schema, model } from 'mongoose'

// En collection definimos como queremos que se cree el nombre de la tabla, ya que por el contrario Mongo solo agregaría 's'
const MedicoSchema = new Schema({
    nombre: {
        type: String,
        required: true
    },
    img: String,
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true
    },
    hospital: {
        type: Schema.Types.ObjectId,
        ref: 'Hospital',
        required: true
    }
});

// Configuración visual para _id. Esto no afecta la base de datos
MedicoSchema.method('toJSON', function() {
    const { __v, ...object } = this.toObject();
    return object;
});

export const Medico = model('Medico', MedicoSchema);