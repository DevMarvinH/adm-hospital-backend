import { Request, Response } from "express";
import * as jwt from 'jsonwebtoken';

export const validarJWT = (req: Request, res: Response, next: any) => {
    // Leer el token
    const token = req.header('authorization');
    if (!token) {
        return res.status(401).json({
            ok: true,
            msg: 'No hay token en la petición'
        });
    }    
    try {
        //Hacemos un split ya que el token incluye la frase Bearer
        //Si la verificación es correcta, entonces se devolverá el payload
        const { uid } = jwt.verify(token.split(' ')[1], process.env.JWT_SECRET) as jwt.JwtPayload; 
        req['uid'] = uid;
        next();
    } catch (error) {
        return res.status(401).json({
            ok: false,
            msg: 'Token incorrecto'
        });
    }
};