import * as jwt from 'jsonwebtoken'

export const generarJWT = (uid: string): Promise<string> => {
    return new Promise((resolve, reject) => {
        const payload: any = {
            uid
        };
        jwt.sign(payload, process.env.JWT_SECRET, {
            expiresIn: '12h'
        }, (error: Error, token: string) => {
            if (error) {
                console.log(error);
                reject('No se pudo generar el JWT');
            } else {
                resolve(token);
            }
        });
    });
}; 