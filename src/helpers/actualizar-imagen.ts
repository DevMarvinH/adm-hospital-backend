import { Hospital } from "../models/hospital.model";
import { Medico } from "../models/medico.model";
import { Usuario } from "../models/usuario.model";
import * as fs from 'fs';

const borrarImagen = (path: string) => {
    if (fs.existsSync(path)) {
        // Borramos imagen anterior
        fs.unlinkSync(path);
    }
};
export const actualizarImagen = async (tipo: string, uid: string, nombreArchivo: string) => {
    let exito: boolean = false;
    switch (tipo) {
        case 'medicos':
            const medico = await Medico.findById(uid);
            if (!medico) {
                console.log('No se encontró el médico por el uid');
                return exito;
            }
            const pathViejo = `./src/uploads/medicos/${ medico.img }`;
            borrarImagen(pathViejo);
            medico.img = nombreArchivo;
            await medico.save();
            exito = true;
            break;
        case 'hospitales':
            const hospital = await Hospital.findById(uid);
            if (!hospital) {
                console.log('No se encontró el hospital por el uid');
                return exito;
            }
            const pathViejoHospital = `./src/uploads/hospitales/${ hospital.img }`;
            borrarImagen(pathViejoHospital);
            hospital.img = nombreArchivo;
            await hospital.save();
            exito = true;
            break;
        case 'usuarios':
            const usuario = await Usuario.findById(uid);
            if (!usuario) {
                console.log('No se encontró el usuario por el uid');
                return exito;
            }
            const pathViejoUsuario = `./src/uploads/usuarios/${ usuario.img }`;
            borrarImagen(pathViejoUsuario);
            usuario.img = nombreArchivo;
            await usuario.save();
            exito = true;
        break;
    }
    return exito;
};