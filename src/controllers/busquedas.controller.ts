import { Request, Response } from "express";
import { Usuario } from "../models/usuario.model";
import { Medico } from "../models/medico.model";
import { Hospital } from "../models/hospital.model";

export const getTodo = async (req: Request, res: Response) => {
    const busqueda: string = req.params['busqueda'];
    const regex = new RegExp(busqueda, 'i');
    try {
        const [usuarios, medicos, hospitales] = await Promise.all([
            Usuario.find({ nombre: regex }),
            Medico.find({ nombre: regex }),
            Hospital.find({ nombre: regex })
        ]);
        res.json({
            ok: true,
            usuarios,
            medicos,
            hospitales
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
};

export const getDocumentoColeccion = async (req: Request, res: Response) => {
    const tabla: string = req.params['tabla'];
    const busqueda: string = req.params['busqueda'];
    const regex = new RegExp(busqueda, 'i');
    try {
        let data: any = [];
        switch (tabla) {
            case 'medicos':
                data = await Medico.find({ nombre: regex })
                                    .populate('usuario', 'nombre img')
                                    .populate('hospital', 'nombre img');
                break;
            case 'hospitales':
                data = await Hospital.find({ nombre: regex })
                                     .populate('usuario', 'nombre img');
                break;
            case 'usuarios':
                data = await Usuario.find({ nombre: regex });
                break;
            default:
                return res.status(400).json({
                    ok: false,
                    msg: 'La tabla tiene que ser medicos/hospitales/usuarios.'
                });
        }
        res.json({
            ok: true,
            resultados: data
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
};