import { Request, Response } from "express";
import { Medico } from "../models/medico.model";

export const getMedicos = async (req: Request, res: Response) => {
    const medicos = await Medico.find()
        .populate('usuario', 'nombre img')
        .populate('hospital', 'nombre img')
    res.json({
        ok: true,
        medicos
    });
};

export const crearMedico = async (req: Request, res: Response) => {
    const uid = req['uid'];
    const medico = new Medico({
        usuario: uid,
        ...req.body
    });
    try {
        const medicoDB = await medico.save();
        res.json({
            ok: true,
            medico: medicoDB
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revise logs'
        });
    }
};

export const actualizarMedico = async (req: Request, res: Response) => {
    res.json({
        ok: true,
        msg: 'actualizarMedico'
    });
};

export const borrarMedico = async (req: Request, res: Response) => {
    res.json({
        ok: true,
        msg: 'borrarMedico'
    });
};