import { Request, Response, json } from "express";
import { Usuario } from "../models/usuario.model";
import * as bcrypt from 'bcryptjs';
import { generarJWT } from "../helpers/jwt";

export const getUsuarios = async (req: Request, res: Response) => {
    const desde = Number(req.query.desde) || 0;
    
    //El método find recibe como objeto los filtros de búsqueda y luego los nombres de los campos que queremos que devuelve junto con el _id
    //Hacemos paginación con límite de 5
    //Usamos desestructuración ya que Promise.All devuelve un arreglo de resultados en base al orden de las promesas
    const [ usuarios, total ] = await Promise.all([
        Usuario.find({}, 'nombre password email google img').skip(desde).limit(5),
        Usuario.count()
    ]);
    res.json({
        ok: true,
        usuarios,
        total
    });
}

export const crearUsuario = async (req: Request, res: Response) => {
    const { password, email } = req.body;
    try {
        const existeEmail = await Usuario.findOne({ email });
        if (existeEmail) {
            return res.status(400).json({
                ok: false,
                msg: 'El correo ya está registrado'
            });
        }
        const usuario = new Usuario(req.body);

        //Encriptar contraseña
        const salt = bcrypt.genSaltSync();
        usuario.password = bcrypt.hashSync(password, salt);

        await usuario.save();

        //Generar Token
        const token = await generarJWT(usuario.id);

        res.status(400).json({
            ok: true,
            token,
            usuario
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
}
export const actualizarUsuario = async (req: Request, res: Response) => {
    const uid = req.params.id;
    try {
        const usuarioDB = await Usuario.findById(uid);
        if (!usuarioDB) {
            return res.status(400).json({
                ok: false,
                msg: 'No existe un usuario por ese id'
            });
        }

        // Actualizaciones
        // Eliminamos los campos que no queremos que se modifiquen a partir de una desestructuración
        const { password, google, email, ...campos } = req.body;
        if (usuarioDB.email !== email) {
            const existeEmail = await Usuario.findOne({ email });
            if (existeEmail) {
                return res.status(400).json({
                    ok: false,
                    msg: 'Ya existe un usuario con ese email'
                });
            }
        }
        //Pasamos new: true, para obtener el registro modificado caso contrario se devolverá el registro como estaba antes de mofificarlo
        campos.email = email;
        const usuarioActualizado = await Usuario.findByIdAndUpdate(uid, campos, { new: true });

        res.json({
            ok: true,
            usuario: usuarioActualizado
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
};

export const borrarUsuario = async (req: Request, res: Response) => {
    const uid = req.params.id;
    try {
        const usuarioDB = await Usuario.findById(uid);
        if (!usuarioDB) {
            return res.status(400).json({
                ok: false,
                msg: 'No existe un usuario por ese id'
            });
        }
        await Usuario.findByIdAndDelete(uid);
        res.json({
            ok: true,
            msg: 'Usuario eliminado'
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
};