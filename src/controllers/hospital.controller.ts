import { Request, Response } from "express";
import { Hospital } from '../models/hospital.model';

export const getHospitales = async (req: Request, res: Response) => {
    const hospitales = await Hospital.find().populate('usuario', 'nombre img');
    res.json({
        ok: true,
        hospitales
    });
};

export const crearHospital = async (req: Request, res: Response) => {
    const uid = req['uid'];
    const hospital = new Hospital({
        ...req.body,
        usuario: uid
    });
    try {
        const hospitalDB = await hospital.save();
        res.json({
            ok: true,
            hospital: hospitalDB
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
};

export const actualizarHospital = async (req: Request, res: Response) => {
    res.json({
        ok: true,
        msg: 'actualizarHospital'
    });
};

export const borrarHospital = async (req: Request, res: Response) => {
    res.json({
        ok: true,
        msg: 'borrarHospital'
    });
};