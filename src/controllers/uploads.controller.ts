import { Request, Response } from "express";
import { UploadedFile } from "express-fileupload";
import { v4 as uuidv4 } from 'uuid';
import { actualizarImagen } from "../helpers/actualizar-imagen";
import * as path from 'path';
import * as fs from 'fs';

export const fileUpload = async (req: Request, res: Response) => {
    const tipo: string = req.params['tipo'];
    const uid: string = req.params['id'];

    // Validar tipo
    const tiposValidos = ['hospitales', 'medicos', 'usuarios'];
    if (!tiposValidos.includes(tipo)) {
        return res.status(400).json({
            ok: false,
            msg: 'No es un médico, usuario u hospital.'
        });
    }

    // Validar que exista un archivo
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).json({
            ok: false,
            msg: 'No hay ningún archivo'
        });
    }

    // Procesar la imagen
    const file = req.files.imagen as UploadedFile;
    const nombreCortado = file.name.split('.');
    const extensionArchivo = nombreCortado[nombreCortado.length - 1];

    // Validar extensión
    const extensionesValidas = ['png', 'jpg', 'jpeg', 'gif'];
    if (!extensionesValidas.includes(extensionArchivo)) {
        return res.status(400).json({
            ok: false,
            msg: 'No es una extensión de archivo permitida.'
        });
    }

    // Generar nombre del archivo
    const nombreArchivo = `${ uuidv4() }.${ extensionArchivo }`;

    // Path para guardar la imagen
    const path = `./src/uploads/${ tipo }/${ nombreArchivo }`;

    // Actualizar base de datos
    actualizarImagen(tipo, uid, nombreArchivo);

    // Mover la imagen
    file.mv(path, error => {
        if (error) {
            console.log(error);
            return res.status(500).json({
                ok: false,
                msg: 'Error al mover la imagen'
            });
        }
        res.json({
            ok: true,
            msg: 'Archivo subido',
            nombreArchivo
        });
    });
};

export const obtenerImagen = (req: Request, res: Response) => {
    const { tipo, foto } = req.params;
    let pathImg = path.join(__dirname, `../uploads/${ tipo }/${ foto }`);
    // Imagen por defecto
    if (!fs.existsSync(pathImg)) {
        pathImg = path.join(__dirname, `../uploads/profile.png`);
    }
    res.sendFile(pathImg);
};