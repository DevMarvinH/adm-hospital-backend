import { Request, Response } from "express";
import { Usuario } from "../models/usuario.model";
import * as bcrypt from 'bcryptjs';
import { generarJWT } from "../helpers/jwt";

export const login = async (req: Request, res: Response) => {
    const { email, password } = req.body;
    try {
        const usuarioDB = await Usuario.findOne({ email });
        // Verificar email
        if (!usuarioDB) {
            return res.status(404).json({
                ok: false,
                msg: 'El email no es válido'
            });
        }
        // Verificar contraseña
        const validPassword = bcrypt.compareSync(password, usuarioDB.password);
        if (!validPassword) {
            return res.status(400).json({
                ok: false,
                msg: 'La contraseña no es válida'
            });
        }
        // Generar token
        const token = await generarJWT(usuarioDB.id);
        res.json({
            ok: true,
            token
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
};

export const googleSignIn = (req: Request, res: Response) => {
    res.json({
        ok: true,
        msg: req.body.token
    });
};